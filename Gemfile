source 'https://rubygems.org'
ruby '2.2.3'

gem 'rails', '4.2.5'
# Used to slim down rails for an api only application
gem 'rails-api'
# # Used for database (pg ftw!)
gem 'pg'
# Used for authentication
gem 'devise'
# Add token based authentication
gem 'simple_token_authentication'
# Used for authorization
gem 'cancancan'
# Used for resource roles
gem 'rolify'
# Used to serialize JSON responses
gem 'active_model_serializers'
# Used for validating date and time
gem 'validates_timeliness'
# Used to preload application to speedup development
gem 'spring'
# Used to generate fake data
gem 'faker', github: 'stympy/faker', branch: 'master'
# Used to dump database
gem 'seed_dump'

group :development, :test do
  gem 'pry'
  gem 'pry-byebug'
  gem 'rspec-rails'
  gem 'factory_girl_rails'
end

group :test do
  gem 'database_cleaner'
  gem 'simplecov'
end

group :production do
  gem 'rails_12factor'
end

# To use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano', :group => :development

