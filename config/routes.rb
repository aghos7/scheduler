Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :users, only: [:index, :show] do
        get :summary, on: :member
      end
      resources :shifts, only: [:index, :show, :create, :update] do
        get :employees, on: :member
        get :managers, on: :member
      end
      get "/users/:employee_id/shifts" => "shifts#user_index"
    end
  end
  devise_for :users, path: "api/v1", defaults: { format: :json } , controllers: { sessions: "api/v1/sessions" }
  root "api/v1/base#index"
end
