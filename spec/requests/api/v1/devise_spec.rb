require 'rails_helper'

RSpec.describe 'api/v1/sign_in', type: :request do
  let(:user) { create(:user, password: 'password') }

  it "allows sign_in with email" do
    user.ensure_authentication_token
    http_login(user.email, 'password')
    post '/api/v1/sign_in', { format: :json }, @env
    expect(response).to have_http_status(:success)
    expect(ActiveSupport::JSON.decode(response.body)).to_not be_nil
    expect(ActiveSupport::JSON.decode(response.body)['authentication_token']).to eq user.authentication_token
  end

  it "allows sign_in with phone" do
    user.ensure_authentication_token
    http_login(user.phone, 'password')
    post '/api/v1/sign_in', { format: :json }, @env
    expect(response).to have_http_status(:success)
    expect(ActiveSupport::JSON.decode(response.body)).to_not be_nil
    expect(ActiveSupport::JSON.decode(response.body)['authentication_token']).to eq user.authentication_token
  end
end
