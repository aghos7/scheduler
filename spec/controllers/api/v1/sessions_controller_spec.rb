require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do
  context "as an employee" do
    login_employee
    describe "#sign_in" do
      it "should have a current_user" do
        expect(subject.current_user).to_not be_nil
      end
      it "should be a employee" do
        expect(subject.current_user.role).to eq :employee
      end
    end
  end
  context "as a manager" do
    login_manager
    describe "#sign_in" do
      it "should have a current_user" do
        expect(subject.current_user).to_not be_nil
      end
      it "should be a manager" do
        expect(subject.current_user.role).to eq :manager
      end
    end
  end
end
