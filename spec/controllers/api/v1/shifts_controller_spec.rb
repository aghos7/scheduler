require 'rails_helper'

RSpec.describe Api::V1::ShiftsController, type: :controller do
  context "as an employee" do
    login_employee
    let(:employee) { subject.current_user }
    let(:employee2) { create(:user, :employee)}

    describe "#index" do
      before do
        4.times do
          create(:shift, :unassigned)
        end
      end
      it "allows listing shifts" do
        get :index
        expect(response).to have_http_status(:success)
        expect(assigns(:shifts)).to_not be_empty
        expect(assigns(:shifts).any?{|shift| shift.employee_id.blank?}).to eq true
        expect(assigns(:shifts).any?{|shift| [employee.id, nil].exclude?(shift.employee_id) }).to eq false

      end
      it "allows listing only assigned shifts" do
        get :index, only_assigned: "true"
        expect(response).to have_http_status(:success)
        expect(assigns(:shifts)).to_not be_empty
        expect(assigns(:shifts).any?{|shift| shift.employee_id.blank?}).to eq false
        expect(assigns(:shifts).all?{|shift| shift.employee_id.present?}).to eq true

      end
      it "allows listing only unassigned shifts" do
        get :index, only_unassigned: "true"
        expect(assigns(:shifts)).to_not be_empty
        expect(assigns(:shifts).all?{|shift| shift.employee_id.blank? }).to eq true
        expect(response).to have_http_status(:success)
      end
      it "allows listing shifts that occur in a time span" do
        start_time = employee.shifts.second.start_time
        end_time = employee.shifts[-1].start_time - 1.minute
        get :index, start_time: start_time, end_time: end_time, only_assigned: "true"
        expect(response).to have_http_status(:success)
        expect(assigns(:shifts)).to_not be_empty
        expect(assigns(:shifts).count).to eq employee.shifts.count - 2
      end
      it "allows listing shifts start on or after a specific time" do
        start_time = employee.shifts.second.start_time
        get :index, start_time: start_time, only_assigned: "true"
        expect(response).to have_http_status(:success)
        expect(assigns(:shifts)).to_not be_empty
        expect(assigns(:shifts).count).to eq employee.shifts.count - 1
      end
      it "allows listing shifts end on or before a specific time" do
        end_time = employee.shifts.first.end_time + 1.minute
        get :index, end_time: end_time, only_assigned: "true"
        expect(response).to have_http_status(:success)
        expect(assigns(:shifts)).to_not be_empty
        expect(assigns(:shifts).count).to eq 1
      end
    end

    describe "#user_index" do
      # As an employee, I want to know when I am working, by being able to see all of the shifts assigned to me.
      it "should allow an employee to list their shifts" do
        get :user_index, employee_id: employee.id
        expect(response).to have_http_status(:success)
        expect(assigns(:shifts)).to_not be_empty
      end

      it "should not allow an employee to list other employees shifts" do
        get :user_index, employee_id: employee2.id
        expect(response).to have_http_status(:forbidden)
      end
    end

    describe "#show" do
      it "should allow an employee to view their shift" do
        get :show, id: employee.shifts.first.id
        expect(response).to have_http_status(:success)
        expect(assigns(:shift)).to_not be_nil
        expect(response.body).to eql(Api::V1::ShiftSerializer.new(employee.shifts.first).to_json)
      end

      it "should not allow an employee to view another employees shift" do
        get :show, id: employee2.shifts.first.id
        expect(response).to have_http_status(:forbidden)
      end
    end

    describe "#employees" do
      # As an employee, I want to know who I am working with, by being able to see the employees that
      # are working during the same time period as me.
      it "should allow an employee to view the employees in their shift" do
        get :employees, id: employee.shifts.first.id
        expect(response).to have_http_status(:success)
        expect(assigns(:employees)).to_not be_empty
      end

      it "should not allow an employee to view the employees of another employees shift" do
        get :employees, id: employee2.shifts.first.id
        expect(response).to have_http_status(:forbidden)
      end
    end

    describe "#managers" do
      # As an employee, I want to be able to contact my managers, by seeing manager contact information for my shifts.
      it "should allow an employee to view the managers in their shift" do
        get :managers, id: employee.shifts.first.id
        expect(response).to have_http_status(:success)
        expect(assigns(:managers)).to_not be_empty
      end

      it "should not allow an employee to view the managers of another employees shift" do
        get :managers, id: employee2.shifts.first.id
        expect(response).to have_http_status(:forbidden)
      end
    end

    describe "#create" do
      it "should not allow an employee to create shifts" do
        post :create, shift: {
          employee_id: employee.id,
          manager_id: create(:user, :manager).id,
          break: Faker::Number.positive(0.25, 0.5),
          start_time: 2.days.ago,
          end_time: 1.days.ago }
        expect(response).to have_http_status(:forbidden)
      end
    end

    describe "#update" do
      it "should not allow an employee to update a shift" do
        put :update, id: employee.shifts.first.id, shift: {
          employee_id: employee.id,
          manager_id: create(:user, :manager).id,
          break: Faker::Number.positive(0.25, 0.5),
          start_time: 3.days.ago,
          end_time: 2.days.ago }
        expect(response).to have_http_status(:forbidden)
      end
    end
  end

  context "as a manager" do
    login_manager
    let(:manager) { subject.current_user }
    let(:employee) { create(:user, :employee)}
    let(:employee2) { create(:user, :employee)}

    describe "#index" do
      # As a manager, I want to see the schedule, by listing shifts within a specific time period.
      it "does allow listing shifts" do
        get :index
        expect(response).to have_http_status(:success)
        expect(assigns(:shifts)).to_not be_empty
      end
    end

    describe "#user_index" do
      # As an employee, I want to know when I am working, by being able to see all of the shifts assigned to me.
      it "should allow a manager to list an employees shifts" do
        get :user_index, employee_id: employee.id
        expect(response).to have_http_status(:success)
        expect(assigns(:shifts)).to_not be_empty
      end
    end

    describe "#show" do
      it "should allow a manager to view an employees shift" do
        get :show, id: employee.shifts.first.id
        expect(response).to have_http_status(:success)
        expect(assigns(:shift)).to_not be_nil
      end
    end

    describe "#employees" do
      it "should allow a manager to view the employees in an employees shift" do
        get :employees, id: employee.shifts.first.id
        expect(response).to have_http_status(:success)
        expect(assigns(:employees)).to_not be_empty
      end
    end

    describe "#managers" do
      it "should allow a manager to view the managers in a shift" do
        get :managers, id: employee.shifts.first.id
        expect(response).to have_http_status(:success)
        expect(assigns(:managers)).to_not be_empty
      end
    end

    describe "#create" do
      # As a manager, I want to schedule my employees, by creating shifts for any employee.
      it "should allow a manager to create shifts" do
        post :create, shift: {
          employee_id: employee.id,
          manager_id: create(:user, :manager).id,
          break: Faker::Number.positive(0.25, 0.5),
          start_time: 2.days.ago,
          end_time: 1.days.ago }
        expect(response).to have_http_status(:success)
      end

      it "should default the manager_id on a shift to the manager that creates it" do
        post :create, shift: {
          employee_id: employee.id,
          break: Faker::Number.positive(0.25, 0.5),
          start_time: 2.days.ago,
          end_time: 1.days.ago }
        expect(response).to have_http_status(:success)
        expect(assigns(:shift).manager.id).to eq manager.id
      end

      it "should not allow a manager to create a shift without a start_time" do
        post :create, shift: {
          employee_id: employee.id,
          break: Faker::Number.positive(0.25, 0.5),
          end_time: 1.days.ago }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    describe "#update" do
      # As a manager, I want to be able to change a shift, by updating the time details.
      it "should allow a manager to update time details of a shift" do
        employee.shifts.first.start_time = 100.days.ago.rfc2822.to_s
        employee.shifts.first.end_time = 99.days.ago.rfc2822.to_s
        put :update, id: employee.shifts.first.id, shift: {
          employee_id: employee.id,
          manager_id: manager.id,
          break: Faker::Number.positive(0.25, 0.5),
          start_time: 4.days.ago,
          end_time: 1.days.ago }
        expect(response).to have_http_status(:success)
        expect(assigns(:shift).start_time.rfc2822).to eq 4.days.ago.rfc2822.to_s
        expect(assigns(:shift).end_time.rfc2822).to eq 1.days.ago.rfc2822.to_s
      end
      # As a manager, I want to be able to assign a shift, by changing the employee that will work a shift.
      it "should allow a manager to assign a shift" do
        expect(employee.shifts.first.employee_id).to eq employee.id
        put :update, id: employee.shifts.first.id, shift: {
          employee_id: employee2.id,
          manager_id: manager.id,
          break: Faker::Number.positive(0.25, 0.5),
          start_time: 3.days.ago,
          end_time: 2.days.ago }
        expect(response).to have_http_status(:success)
        expect(assigns(:shift).employee_id).to eq employee2.id
      end

      it "should not allow a manager to create a shift without a start_time" do
        expect(employee.shifts.first.employee_id).to eq employee.id
        put :update, id: employee.shifts.first.id, shift: {
          employee_id: employee2.id,
          manager_id: manager.id,
          break: Faker::Number.positive(0.25, 0.5),
          start_time: nil,
          end_time: 2.days.ago }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
