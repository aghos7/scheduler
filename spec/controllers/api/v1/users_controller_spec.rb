require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  context "as an employee" do
    login_employee
    let(:employee) { subject.current_user }
    let(:employee2) { create(:user, :employee)}

    describe "#index" do
      it "does not allow employees to list all users" do
        get :index
        expect(response).to have_http_status(:forbidden)
      end
    end

    describe "#show" do
      it "allows an employee to show their own details" do
        get :show, id: employee.id
        expect(response).to have_http_status(:success)
        expect(assigns(:user)).to eql(employee)
        expect(response.body).to eql(Api::V1::UserSerializer.new(employee).to_json)
      end
      it "does not allow an employee to show another employees details" do
        get :show, id: employee2.id
        expect(response).to have_http_status(:forbidden)
      end
    end

    describe "#summary" do
      # As an employee, I want to know how much I worked, by being able to get a summary of hours worked for each week.
      it "should create a summary of hours worked for each week" do
        get :summary, id: employee.id
        expect(response).to have_http_status(:success)
        expect(assigns(:summary)).to_not be_nil
      end
      it "should not support bad summary types" do
        get :summary, id: employee.id, type: "badbadtype"
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  context "as a manager" do
    login_manager
    let(:manager) { subject.current_user }
    let(:employee) { create(:user, :employee)}

    describe "#index" do
      it "allows managers to list all users" do
        get :index
        expect(response).to have_http_status(:success)
        expect(assigns(:users)).to_not be_empty
        expect(assigns(:users).count).to eq User.all.count
      end
    end

    # As a manager, I want to contact an employee, by seeing employee details.
    describe "#show" do
      it "allows managers to show a users details" do
        get :show, id: employee.id
        expect(response).to have_http_status(:success)
        expect(assigns(:user)).to eql(employee)
        expect(response.body).to eql(Api::V1::UserSerializer.new(employee).to_json)
      end

      it "returns a status of not_found for invalid users" do
        get :show, id: 0
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
