require 'rails_helper'

FactoryGirl.factories.map(&:name).each do |factory_name|
  describe "The #{factory_name} factory" do
    it 'build is valid' do
      expect(build(factory_name)).to be_valid
    end
    it 'create is valid' do
      expect(create(factory_name)).to be_valid
    end
  end
end
