FactoryGirl.define do
  factory :shift do
    association :manager, factory: [:user, :manager]
    association :employee, factory: [:user, :employee]
    self.break              { Faker::Number.positive(0.25, 0.5) }
    start_time              { Faker::Time.between(5.days.ago, 5.days.from_now) }
    end_time                { |shift| Faker::Time.between(shift.start_time + 1.hours, shift.start_time + 8.hours) if shift.start_time.present?}

    trait :unassigned do
      employee nil
    end
  end
end
