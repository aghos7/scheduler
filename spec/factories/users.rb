FactoryGirl.define do
  factory :user do
    first_name              { Faker::Name.first_name }
    last_name               { Faker::Name.first_name }
    sequence(:email)        { |n| "#{Faker::Internet.free_email}#{n}" }
    password                { Faker::Internet.password(8) }
    sequence(:phone)        { |n| "#{Faker::PhoneNumber.phone_number}#{n}" }

    trait :employee do
      role :employee
      shift_count 5
    end

    trait :manager do
      role :manager
      shift_count 0
    end

    transient do
      shift_count 0
    end

    after(:create) do |user, evaluator|
      if user.role == :employee
        create_list(:shift, evaluator.shift_count, employee: user, manager: create(:user, :manager))
      end
    end
  end
end
