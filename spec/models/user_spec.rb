require 'rails_helper'

RSpec.describe User, type: :model do
  describe "user roles" do
    it "properly assigns employee role" do
      expect(create(:user, :employee).role).to eq :employee
    end
    it "properly assigns manager role" do
      expect(create(:user, :manager).role).to eq :manager
    end
    it "defaults to employee role" do
      expect(create(:user).role).to eq :employee
    end
    it "allows setting of role" do
      user = build(:user)
      user.role = :manager
      user.save!
      expect(user.role).to eq :manager
    end
    it "only allows valid roles" do
      user = build(:user, role: :admin)
      user.valid?
      expect(user.errors[:role]).to include(/is not included in the list/)
    end
  end

  describe "email or phone should be defined" do
    it "allows just a phone" do
      expect(create(:user, phone: "123", email: nil).email).to be_nil
    end

    it "allows just an email" do
      expect(create(:user, email: "foo@bar.com", phone: nil).phone).to be_nil
    end

    it "requires email or phone to have a value" do
      user = build(:user, email: nil, phone: nil)
      user.valid?
      expect(user.errors[:email]).to include(/can't be blank/)
      expect(user.errors[:phone]).to include(/can't be blank/)
    end

    it "does not allow an email to match an existing users phone number" do
      create(:user, email: "111-222-3333", phone: "111-222-3334")
      user = build(:user, email: "foo@bar.com", phone: "111-222-3333")
      expect(user.valid?).to eq false
      expect(user.errors[:email]).to include(/can't match another users phone number/)
    end
  end

  it "concats first and last name" do
    expect(create(:user, first_name: "first", last_name: "last").name).to eq "first last"
  end

  it "allows employees to have shifts" do
    expect(create(:user, shift_count: 5).shifts.count).to eq 5
  end
end
