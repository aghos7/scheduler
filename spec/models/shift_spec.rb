require 'rails_helper'

RSpec.describe Shift, type: :model do
  it "requires a manager" do
    shift = build(:shift, manager: nil)
    shift.valid?
    expect(shift.errors[:manager]).to include(/can't be blank/)
  end

  it "does not require an employee" do
    expect(create(:shift, employee: nil)).to be_valid
  end

  describe "start and end time" do
    it "does not allow an empty start_time" do
      shift = build(:shift, start_time: nil)
      shift.valid?
      expect(shift.errors[:start_time]).to_not be_empty
    end
    it "does not allow an empty end_time" do
      shift = build(:shift, end_time: nil)
      shift.valid?
      expect(shift.errors[:end_time]).to_not be_empty
    end
    it "require start_time to be before end_time" do
      shift = build(:shift, start_time: 1.days.ago, end_time: 2.days.ago)
      shift.valid?
      expect(shift.errors[:start_time]).to_not be_empty
    end
  end
end
