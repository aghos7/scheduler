RSpec.configure do |config|

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each, :truncate => true) do
    DatabaseCleaner.strategy = :truncation, {reset_ids: true}
    DatabaseCleaner.clean
  end
end
