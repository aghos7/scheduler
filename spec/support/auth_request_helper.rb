module AuthRequestHelper
  # pass the @env along with your request, eg:
  # GET '/labels', {}, @env
  def http_login(username='username', password='password')
    @env ||= {}
    @env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(username, password)
  end
end
