module ControllerMacros
  def login_employee
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      employee = FactoryGirl.create(:user, :employee)
      sign_in :user, employee
      @request.headers['X-USER-ID'] = employee.id
      @request.headers['X-USER-TOKEN'] = employee.authentication_token
    end
  end

  def login_manager
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      manager = FactoryGirl.create(:user, :manager)
      sign_in :user, manager
      @request.headers['X-USER-ID'] = manager.id
      @request.headers['X-USER-TOKEN'] = manager.authentication_token
    end
  end
end
