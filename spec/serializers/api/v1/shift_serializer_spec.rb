require 'rails_helper'

RSpec.describe Api::V1::ShiftSerializer, type: :serializer do
  describe "Serialize a shift" do
    let(:shift) { create(:shift) }
    let(:serialized_shift) { Api::V1::ShiftSerializer.new(shift).to_json }

    subject do
      JSON.parse(serialized_shift)["shift"]
    end

    it "has the correct number of attributes" do
      expect(subject.keys.count).to eq 8
    end
    it "has an id" do
      expect(subject["id"]).to eq shift.id
    end
    it "has a manager id" do
      expect(subject["manager_id"]).to eq shift.manager_id
    end
    it "has an employee id" do
      expect(subject["employee_id"]).to eq shift.employee_id
    end
    it "has a break" do
      expect(subject["break"]).to eq shift.break
    end
    it "has a start time which is formated according to rfc2822" do
      expect(subject["start_time"]).to eq shift.start_time.rfc2822
    end
    it "has an end time which is formated according to rfc2822" do
      expect(subject["end_time"]).to eq shift.end_time.rfc2822
    end
    it "has a create at time which is formated according to rfc2822" do
      expect(subject["created_at"]).to eq shift.created_at.rfc2822
    end
    it "has an updated at time which is formated according to rfc2822" do
      expect(subject["updated_at"]).to eq shift.updated_at.rfc2822
    end
  end
end
