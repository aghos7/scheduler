require 'rails_helper'

RSpec.describe Api::V1::UserSerializer, type: :serializer do
  describe "Serialize an employee" do
    let(:employee) { create(:user, :employee) }

    subject do
      JSON.parse(Api::V1::UserSerializer.new(employee).to_json)["user"]
    end

    it "has the correct number of attributes" do
      expect(subject.keys.count).to eq 7
    end
    it "has an id" do
      expect(subject["id"]).to eq employee.id
    end
    it "has a name" do
      expect(subject["name"]).to eq employee.name
    end
    it "has a role" do
      expect(subject["role"]).to eq employee.role.to_s
    end
    it "has an email" do
      expect(subject["email"]).to eq employee.email
    end
    it "has a phone" do
      expect(subject["phone"]).to eq employee.phone
    end
    it "has a create at time which is formated according to rfc2822" do
      expect(subject["created_at"]).to eq employee.created_at.rfc2822
    end
    it "has an updated at time which is formated according to rfc2822" do
      expect(subject["updated_at"]).to eq employee.updated_at.rfc2822
    end
  end
end
