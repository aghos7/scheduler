class User < ActiveRecord::Base
  ALLOWED_ROLES = [:employee, :manager]

  rolify
  acts_as_token_authenticatable
  devise :database_authenticatable

  attr_accessor :login
  # Shifts that are assigned to the user as an employee
  has_many :shifts, -> { Shift.order_desc }, foreign_key: :employee_id
  # Shifts that are assigned to the user as a manager
  has_many :manager_shifts, -> { Shift.order_desc }, class_name: "Shift", foreign_key: :manager_id
  # The role must be either employee or manager
  validates :role, inclusion: { in: User::ALLOWED_ROLES }
  # At least one of phone or email must be defined
  # Should probably validate format of email
  validates :email, presence: true, unless: :phone?
  # Should probably validate format of phone
  validates :phone, presence: true, unless: :email?
  validate :validate_email_phone_unique_combination

  after_initialize :default_values

  # Finds the user by either phone number or email for authentication
  # Sure hope no one uses an email that matches another users phone number :p
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where("email = :login OR phone = :login", login: login).first
    else
      where(conditions.to_hash).first
    end
  end

  # Gets the users fullname
  def name
    [first_name, last_name].join(' ')
  end

  # Gets the users role
  def role
    self.try(:roles).try(:first).try(:name).try(:to_sym)
  end

  # Sets the users role
  def role=(new_role)
    self.roles = []
    self.add_role(new_role)
  end

  # Initialize default values for a user
  def default_values
    self.role ||= :employee
  end
  private :default_values

  # Since the user may login with their email or phone number we need to validate
  # that a user doesn't exist with an email of another users phone number
  def validate_email_phone_unique_combination
    if phone.present? && User.where(email: phone).exists?
      errors.add(:email, "can't match another users phone number")
    end
  end
  private :validate_email_phone_unique_combination
end
