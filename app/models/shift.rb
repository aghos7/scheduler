class Shift < ActiveRecord::Base
  resourcify
  belongs_to :employee, class_name: "User"
  belongs_to :manager, class_name: "User"

  validates :manager, presence: true
  # Both start_time and end_time are required
  validates :start_time, timeliness: { before: :end_time, allow_blank: false }
  validates :end_time, timeliness: { after: :start_time, allow_blank: false }

  # Order shifts by start_time then end_time ascending
  scope :order_desc, -> { order(start_time: :asc).order(end_time: :asc) }
  # Filter shifts to those which are assigned to any employee
  scope :assigned, -> { where.not(employee_id: nil) }
  # Filter shifts to those which are not assigned to any employee
  scope :unassigned, -> { where(employee_id: nil) }
  # Filter shifts to those which are assigned to a specific employee
  scope :by_employee_id, ->(employee_id) { where(employee_id: employee_id) }
  # Filter shifts to those which are managed by a specific manager
  scope :by_manager_id, ->(manager_id) { where(manager_id: manager_id) }
  # Filter shifts to those which overlap with the specfied time range
  scope :overlap, ->(start_time, end_time) {
    if start_time.present? && end_time.present?
      where(":start_time < shifts.end_time AND shifts.start_time < :end_time", start_time: start_time, end_time: end_time)
    end
  }
  # Filter shifts to those which are entirely within the specfied time range
  scope :between, ->(start_time, end_time) { start_after(start_time).end_before(end_time) }
  # Filter shifts to those which start after the specified time
  scope :start_after, ->(time) { where("shifts.start_time >= ?", time) if time.present? }
  # Filter shifts to those which end before the specified time
  scope :end_before, ->(time) { where("shifts.end_time <= ?", time) if time.present? }

  # Get the duration of a shift in hours
  # @returns [Float] The duration
  def duration
    (self.end_time - self.start_time) / 3600.0
  end
end
