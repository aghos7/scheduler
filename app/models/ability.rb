class Ability
  include CanCan::Ability

  # Initialize the abilities a user has
  # @required [User] user User to requesting abilities
  def initialize(user)
    user ||= User.new(role: :guest)
    if user.has_role? :manager
      can :manage, :all
    elsif user.has_role? :employee
      can :index, Shift, employee_id: [user.id, nil]
      can :user_index, Shift, employee_id: [user.id, nil]
      can :user_index_multiple, ActiveRecord::Relation do |arr|
        arr.present? && arr.all? { |el| can?(:user_index, el) }
      end
      can :show, Shift, employee_id: [user.id, nil]
      can :employees, Shift, employee_id: [user.id, nil]
      can :managers, Shift, employee_id: [user.id, nil]
      can :show, User, id: user.id
      can :summary, User, id: user.id
    end
  end
end
