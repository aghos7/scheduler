class Api::V1::ShiftsSummaryService
  attr_accessor :shifts

  def initialize(shifts)
    self.shifts = shifts
  end

  # Generate a weekly hours summary
  # @note Assumes the week starts on monday
  # @note A shift is considered to belong in a week if the start time is within the week.
  # Thus if a shift spans weeks, its time will not be split
  def weekly_hours_summary
    results = []
    # Week number of the week-based year (01..53)
    weekly = ->(shift) { [shift.start_time.year, shift.start_time.strftime('%V')] }
    # Group shifts by year and week
    shifts.group_by(&weekly).each do |week, grouped_shifts|
      # Add all the hours of the shifts, less break time
      hours = grouped_shifts.reduce(0){ |total, shift| total + (shift.duration - shift.break) }
      start_time = grouped_shifts.first.start_time.beginning_of_week
      end_time = start_time.end_of_week
      results << HoursSummary.new(start_time: start_time, end_time: end_time, hours: hours)
    end
    results
  end
end
