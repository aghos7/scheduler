class Api::V1::BaseController < ApplicationController
  include ActionController::Serialization
  include ActionController::ImplicitRender
  include ActionController::RespondWith
  include ActionController::StrongParameters
  include CanCan::ControllerAdditions
  acts_as_token_authentication_handler_for User, fallback_to_devise: false
  respond_to :json

  rescue_from CanCan::AccessDenied do |exception|
    render json: { error: "Access denied", message: exception.message }, status: :forbidden
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render json: { error: "Record not found", message: exception.message }, status: :not_found
  end

  def index
    redirect_to "/coverage/index.html"
  end
end
