class Api::V1::ShiftsController < Api::V1::BaseController
  include Api::V1::ShiftFilter
  load_and_authorize_resource :shift, param_method: :shift_params, except: [:user_index]

  # GET /api/v1/shifts
  # List shifts the current user may access
  # @optional [DateTime] start_time Start time for the search window
  # @optional [DateTime] end_time [DateTime] End time for the search window
  # @optional [Boolean] only_unassigned Only List unassigned shifts
  # @optional [Boolean] only_assigned  Only include assigned shifts in the results
  # @response [Array<Shift>] shifts List of shifts
  def index
    @shifts = filter_shifts(@shifts)
    render json: @shifts
  end

  # GET /api/v1/user/:employee_id/shifts
  # List shifts the specified employee may access
  # @optional [DateTime] start_time Start time for the search window
  # @optional [DateTime] end_time End time for the search window
  # @response [Array<Shift>] shifts List of shifts
  def user_index
    @shifts = filter_shifts(Shift.by_employee_id(params[:employee_id]))
    authorize! :user_index_multiple, @shifts
    render json: @shifts
  end

  # GET /api/v1/shifts/:id
  # View a shift
  # @required [Integer] id Identifier of the shift
  # @response [Shift] The requested shift
  def show
    render json: @shift
  end

  # GET /api/v1/shifts/:id/employees
  # Retrieve employees with shifts during the same time period as the specified shift
  # @required [Integer] id Indentifier of a shift
  # @response [Array<User>] The employees with shifts in the same time period
  def employees
    @employees = User.joins(:shifts).merge(Shift.overlap(@shift.start_time, @shift.end_time)).uniq
    if current_user.role == :employee
      @employees = @employees.where.not(id: current_user.id)
    end
    render json: @employees, root: 'users', each_serializer: Api::V1::UserSerializer
  end

  # GET /api/v1/shifts/:id/managers
  # Retrieve managers assigned during the same time period as the specified shift
  # @required [Integer] id Indentifier of a shift
  # @response [Array<User>] The managers assigned during a shift
  def managers
    @managers = User.joins(:manager_shifts).merge(Shift.overlap(@shift.start_time, @shift.end_time)).uniq
    render json: @managers, root: 'users', each_serializer: Api::V1::UserSerializer
  end

  # POST /api/v1/shifts
  # Create a shift
  # @optional [Integer] employee_id User Id of the employee assigned to the shift
  # @optional [Integer] manager_id User Id of the manager assigned to the shift
  # @optional [float] break The break length in hours
  # @required [DateTime] start_time The start time of the shift
  # @required [DateTime] end_time The end time of the shift
  # @response [Shift] The created shift
  # @note If the manager_id is nil, the id of the creator shall be used
  def create
    # the manager_id should always default to the manager that created the shift
    @shift.manager ||= current_user
    if @shift.save
      render json: @shift, status: :created
    else
      render json: @shift.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/shifts/:id
  # @optional [Integer] employee_id User Id of the employee assigned to the shift
  # @optional [Integer] manager_id User Id of the manager assigned to the shift
  # @optional [float] break The break length in hours
  # @required [DateTime] start_time The start time of the shift
  # @required [DateTime] end_time The end time of the shift
  # @response [Shift] The updated shift
  def update
    if @shift.update(shift_params)
      head :no_content
    else
      render json: @shift.errors, status: :unprocessable_entity
    end
  end

  # Parameters that are permited when creating or updating a shift
  def shift_params
    params.require(:shift).permit(:employee_id, :manager_id, :break, :start_time, :end_time, :shift_user_id)
  end
  private :shift_params
end
