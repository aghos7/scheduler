class Api::V1::UsersController < Api::V1::BaseController
  include Api::V1::ShiftFilter
  load_and_authorize_resource :user, param_method: :user_params

  # GET /api/v1/users
  # List the users the current user may access
  # @response [Array<User>] users List of users
  def index
    render json: @users
  end

  # GET /api/v1/users/:id
  # View a user
  # @required [Integer] id User Id of the user
  # @response [User] The requested user
  def show
    render json: @user
  end

  # GET /api/v1/users/:id/summary
  # View a summary of a user
  # @required [Integer] id User Id of the user
  # @optional [String] type Type of summary to generate
  # @optional [DateTime] start_time Start time for the search window
  # @optional [DateTime] end_time [DateTime] End time for the search window
  # @note Supported types include: "weekly_hours"
  # @response [Summary] summary The summary requested
  def summary
    type = params[:type] || "weekly_hours"
    if type == "weekly_hours"
      @summary = Api::V1::ShiftsSummaryService.new(filter_shifts(@user.shifts)).weekly_hours_summary
      render json: @summary, root: type.to_s, each_serializer: Api::V1::HoursSummarySerializer
    else
      render json: { error: "Unknown type", message: "#{type.to_s.humanize} summary type is not yet supported" },
        status: :unprocessable_entity
    end
  end

  # Parameters that are permited when creating or updating a user
  # @note Currently unused as creating users is not supported
  def user_params
    params.require(:user)
  end
  private :user_params
end
