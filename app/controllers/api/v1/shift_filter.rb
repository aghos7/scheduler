module Api::V1::ShiftFilter
  extend ActiveSupport::Concern

  # Filter shifts based on request parameters
  # @required [Array<Shift>] Collection of shifts to filter
  # @options [Hash] Options to use when filtering shifts, defaults to params
  def filter_shifts(shifts, options=params)
    return shifts unless shifts.present?
    # This is ugly and should be refactored
    if (options[:only_unassigned] == "true")
      shifts = shifts.unassigned
    elsif (options[:only_assigned].present? && options[:only_assigned] == "true")
      shifts = shifts.assigned
    end

    if (options[:start_time].present? && options[:end_time].present?)
      shifts = shifts.overlap(options[:start_time], options[:end_time])
    elsif (options[:start_time].present?)
      shifts = shifts.start_after(options[:start_time])
    elsif (options[:end_time].present?)
      shifts = shifts.end_before(options[:end_time])
    end
    shifts.order_desc
  end
  private :filter_shifts
end
