class Api::V1::SessionsController < Devise::SessionsController
  include ActionController::ImplicitRender
  include ActionController::RespondWith
  respond_to :json
end
