class Api::V1::HoursSummarySerializer < ActiveModel::Serializer
  attributes :start_time, :end_time, :hours

  def start_time
    object.start_time.rfc2822
  end

  def end_time
    object.end_time.rfc2822
  end

  def hours
    object.hours.round(4)
  end
end
