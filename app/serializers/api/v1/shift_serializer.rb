class Api::V1::ShiftSerializer < ActiveModel::Serializer
  attributes :id, :manager_id, :employee_id, :break, :start_time, :end_time, :created_at, :updated_at

  def start_time
    object.start_time.rfc2822
  end

  def end_time
    object.end_time.rfc2822
  end

  def created_at
    object.created_at.rfc2822
  end

  def updated_at
    object.updated_at.rfc2822
  end
end
