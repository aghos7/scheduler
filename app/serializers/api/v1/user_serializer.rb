class Api::V1::UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :role, :email, :phone, :created_at, :updated_at

  def created_at
    object.created_at.rfc2822
  end

  def updated_at
    object.updated_at.rfc2822
  end
end
