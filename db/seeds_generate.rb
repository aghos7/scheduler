# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
case Rails.env
when "development"
  dummy_user_details = [
    { first_name: "Lucas",
      last_name: "Anderson",
      role: :manager,
      email: "aghos7@gmail.com",
      phone: "612-555-0001",
      password: "password",
      authentication_token: "dWDwnTtjaP" },
    { first_name: "Paul",
      last_name: "DeBettignies",
      role: :employee,
      email: "paul@wheniwork.com",
      phone: "612-555-0002",
      password: "password",
      authentication_token: "h8t2i6aNvCWnMuddPeo9" },
    { first_name: "Daniel",
      last_name: "Olfelt",
      role: :manager,
      email: "dan@wheniwork.com",
      phone: "612-555-0003",
      password: "password" },
    { first_name: "Sujan",
      last_name: "Patel",
      role: :employee,
      email: "sujan@wheniwork.com",
      phone: "612-555-0004",
      password: "password" },
    { first_name: "Jeff",
      last_name: "Imm",
      role: :employee,
      email: "jeff@wheniwork.com",
      phone: "612-555-0005",
      password: "password" },
    { first_name: "Woody",
      last_name: "Gilk",
      role: :employee,
      email: "woody@wheniwork.com",
      phone: "612-555-0006",
      password: "password" },
    { first_name: "Garret",
      last_name: "Voight",
      role: :employee,
      email: "garret@wheniwork.com",
      phone: "612-555-0007",
      password: "password" },
  ]

  dummy_user_details.map { |user_details| User.create(user_details) }

  managers = User.with_role(:manager)
  employees = [nil] | User.with_role(:employee)
  # employees = User.with_role(:employee)

  2000.times do
    start_time = Faker::Time.between(10.days.ago, 10.days.from_now)
    end_time = Faker::Time.between(start_time + 1.hours, start_time + 8.hours)
    shift_details = {
      manager: managers.sample,
      employee: employees.sample,
      break: Faker::Number.positive(0.25, 0.5),
      start_time: start_time,
      end_time: end_time
    }
    Shift.create(shift_details)
  end
end
