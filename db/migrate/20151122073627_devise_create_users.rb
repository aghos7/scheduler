class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      ## Database authenticatable
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      ## token_authenticatable
      t.string :authentication_token
      t.string :encrypted_password, null: false, default: ""
      t.timestamps null: false
    end
    add_index :users, :email, :unique => true
    add_index :users, :phone, :unique => true
  end
end
