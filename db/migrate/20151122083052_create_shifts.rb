class CreateShifts < ActiveRecord::Migration
  def change
    create_table :shifts do |t|
      t.integer :manager_id
      t.integer :employee_id
      t.float :break
      t.timestamp :start_time
      t.timestamp :end_time
      t.timestamps null: false
    end
    add_index :shifts, :manager_id
    add_index :shifts, :employee_id
    add_index :shifts, :start_time
    add_index :shifts, :end_time
  end
end
