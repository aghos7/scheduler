# REST Scheduler API
The finest API to schedule employees on the planet!

Available for a limited time on [Heroku](https://floating-forest-9903.herokuapp.com)!

It supports all of these great features!!
###### Authentication
- HTTP Authentication via email/phone and password.
- Token Authentication via HTTP request headers.

###### Everything an employee could need
- I want to know when I am working, by being able to see all of the shifts assigned to me.
- I want to know who I am working with, by being able to see the employees that are working during the same time period as me.
- I want to know how much I worked, by being able to get a summary of hours worked for each week.
- I want to be able to contact my managers, by seeing manager contact information for my shifts.

###### Everything a manager could ask for
- I want to schedule my employees, by creating shifts for any employee.
- I want to see the schedule, by listing shifts within a specific time period.
- I want to be able to change a shift, by updating the time details.
- I want to be able to assign a shift, by changing the employee that will work a shift.
- I want to contact an employee, by seeing employee details.

#### Checkout all the cool features using the supplied [Postman](https://www.getpostman.com/) collections!

Postman collection for [localhost](scheduler_heroku.json.postman_collection).

Postman collection for [Heroku](scheduler_heroku.json.postman_collection).

#### Thoughts
After completing this solution to the problem, I am not very happy with my solution for the following reasons:
- It is a little messy
- It needs more comments
- The internal relationships are specific to the data structures defined in the problem and not a general solution.
- While the tests cover ~98.5% of the code, validation of the output is not very thorough.
- It may be revealing too much sensitive personal information to other employees.
- When listing coworkers during a shift, it would have been nice to add the time overlaps to the response.
- A simple UI would have been nice.  Postman is great, but I could have done better.
- It is not in PHP :(

I completed this using rails-api, a slimmer version or rails.  While SparkPHP was strongly suggested, I believe I was able to achieve a better result within the time allowed using frameworks I am more familiar with.  I know I will ramp back up on PHP very quickly.  I can redo the challenge using SparkPHP, but I would need more time to catch up with the appropriate PHP tools and best practices.

#### Users
The following users are available:

```
  { id: 1
    name: "Lucas Anderson", 
    email: "aghos7@gmail.com", 
    phone: "612-555-0001", 
    role: :manager, 
    authentication_token: "dWDwnTtjaP", 
    password: "password" },
  { id: 2
    name: "Paul DeBettignies", 
    email: "paul@wheniwork.com", 
    phone: "612-555-0002", 
    role: :employee, 
    authentication_token: "h8t2i6aNvCWnMuddPeo9", 
    password: "password" },
  { id: 3
    name: "Daniel Olfelt", 
    email: "dan@wheniwork.com", 
    phone: "612-555-0003", 
    role: :manager, 
    authentication_token: "vdgzFERMvGE4usc-ZvU-", 
    password: "password" },
  { id: 4
    name: "Sujan Patel", 
    email: "sujan@wheniwork.com", 
    phone: "612-555-0004", 
    role: :employee, 
    authentication_token: "-sp_kpVZdVH4_GE6zx38", 
    password: "password"},
  { id: 5
    name: "Jeff Imm", 
    email: "jeff@wheniwork.com", 
    phone: "612-555-0005", 
    role: :employee, 
    authentication_token: "EiXkuN2E6nsc91a3cvrU", 
    password: "password"},
  { id: 6
    name: "Woody Gilk", 
    email: "woody@wheniwork.com", 
    phone: "612-555-0006", 
    role: :employee, 
    authentication_token: "ofBRwG2gUusbeSfhwBAQ", 
    password: "password"},
  { id: 7
    name: "Garret Voight", 
    email: "garret@wheniwork.com", 
    phone: "612-555-0007", 
    role: :employee, 
    authentication_token: "dhT9SZb8iXzhsyF1mhSK", 
    password: "password"}
```

#### Authentication
A user may sign-in to retrieve their user id and authorization token via HTTP Basic Auth.

```
username: <user email or phone>
password: <user password>
```
The response of the sign-in request will include the users' id and authentication_token.

Each subsequent request must include the following headers to authenticate the user.

```
X-USER-ID: <user id>
X-USER-TOKEN: <user authentication_token>
```



#### Requirements
[Ruby 2.2.3](https://www.ruby-lang.org/en/downloads/)

[PostgreSQL](http://www.postgresql.org/download/)

Installation
------------
##### Install Ruby
I recommend installing [RVM](https://rvm.io/rvm/install).

```
curl -L https://get.rvm.io | bash -s stable
```

Then install ruby 2.2.3 with

```
rvm install 2.2.3
```

##### Install PostgreSQL
I recommend installing [Homebrew](http://brew.sh/) for OS X.

```
brew install postgres
```

Clone the repo and change into the directory it was cloned into.

##### Install Bundler Gem
```
gem install bundler
```

Setup
------------

```
bundle
```
```
rake db:setup
```

Running
------------
```
rails -s
```

It will now be available at <http://localhost:3000>!

The root route redirects to test coverage!!

Available Routes
------------
```
Verb   URI Pattern                            Function
GET    /api/v1/users                          List all users
GET    /api/v1/users/:id                      Show a specific user
GET    /api/v1/users/:id/summary              Display a summary of hours worked per week for a user
  - Optional URL Parameters -
    start_time - Start time for the search window. i.e. 2015-12-11 00:56:29
    end_time - End time for the search window. i.e. 2015-12-11 00:56:29

GET    /api/v1/users/:id/shifts               List a users shifts
  - Optional URL Parameters -
    start_time - Start time for the search window. i.e. 2015-12-11 00:56:29
    end_time - End time for the search window. i.e. 2015-12-11 00:56:29

GET    /api/v1/shifts                         List shifts
  - Optional URL Parameters -
    start_time - Start time for the search window. i.e. 2015-12-11 00:56:29
    end_time - End time for the search window. i.e. 2015-12-11 00:56:29
    only_unassigned - Only List unassigned shifts. i.e. true or false
    only_assigned - Only include assigned shifts in the results i.e. true or false

GET    /api/v1/shifts/:id                     Show a specific shift
GET    /api/v1/shifts/:id/employees           List the employees working within a shifts time span
GET    /api/v1/shifts/:id/managers            List the managers working within a shifts time span
POST   /api/v1/shifts                         Create shifts
    shift: {
      employee_id: <user id of employee, i.e. >,
      manager_id: <user id of manager>,
      break: <break length in hours, i.e. 0.5>,
      start_time: <start time>,
      end_time: <end time> 
    }

PUT    /api/v1/shifts/:id                     Update a shift  
  id: <id of shift>,
  shift: {
    employee_id: <user id of employee>,
    manager_id: <user id of manager>,
    break: <break length in hours>,
    start_time: <start time>,
    end_time: <end time> 
  }

POST   /api/v1/sign_in                        Sign in with email/phone and password
DELETE /api/v1/sign_out                       Sign out
GET    /                                      Show code coverage of automated tests
```

Testing
------------
Run
```
rake db:test:prepare
rspec .
```